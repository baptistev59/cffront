import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ParametreComponent } from './parametre.component';


@NgModule({
  declarations: [ParametreComponent],
  imports: [
    BrowserModule, CommonModule, FormsModule, ReactiveFormsModule, NgbModule
  ],
  exports: [ParametreComponent],
  bootstrap: [ParametreComponent]
})
export class ParametreModule { }
