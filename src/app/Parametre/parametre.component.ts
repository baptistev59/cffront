import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Centre } from '../shared/models/centre.model';
import { CentreServices } from '../shared/services/centre.services';
import { LieuxService } from '../shared/services/lieux.service';
import { Lieux } from '../shared/models/lieux.model';

@Component({
  selector: 'app-parametre',
  templateUrl: './parametre.component.html',
  styleUrls: ['./parametre.component.css']
})
export class ParametreComponent implements OnInit {
  public model: any;
  public centre: Centre;
  public lieux: Lieux;
  public listLieux: Array<Lieux>;

  constructor(
    private lieuxService: LieuxService,
    private centreservice: CentreServices
  ) {
    this.centre = new Centre();
    this.lieux = new Lieux();
    this.listLieux = [];
  }

  monForm: FormGroup;

  get libelle() {
    return this.monForm.get('libelle');
  }
  get slogan() {
    return this.monForm.get('slogan');
  }
  get tel() {
    return this.monForm.get('tel');
  }
  get fax() {
    return this.monForm.get('fax');
  }
  get email() {
    return this.monForm.get('email');
  }
  get site() {
    return this.monForm.get('site');
  }
  get siret() {
    return this.monForm.get('siret');
  }
  get lieu() {
    return this.monForm.get('lieu');
  }
  ngOnInit() {
    this.createForm();
    this.listeLieux();
    this.recupCentre();
  }
  createForm() {
    this.monForm = new FormGroup({
      libelle: new FormControl('', Validators.required),
      slogan: new FormControl('', Validators.required),
      tel: new FormControl('', Validators.required),
      lieu: new FormControl('', Validators.required),
      fax: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      site: new FormControl('', Validators.required),
      siret: new FormControl('', Validators.required)
    });
  }

  onSubmitt() {
    this.centreservice
      .addCentre(this.centre)
      .subscribe(result => this.centre);
  }

  listeLieux() {
    this.lieuxService.findAll().subscribe(result => {
      this.listLieux = result;
    });
  }

  recupCentre() {
    this.centreservice.findById(1).subscribe(result => {
      this.centre = result;
    });
  }

  modifLieu(id: number) {
    this.lieuxService.findById(id).subscribe(result => {
      this.centre.lieu = result;
    });

  }

}
