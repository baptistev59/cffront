import { Component, OnInit } from '@angular/core';
import { Personnes } from '../shared/models/personnes.model';
import { PersonnesServices } from '../shared/services/personnes.services'
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-createpers',
  templateUrl: './createpers.component.html',
  styleUrls: ['./createpers.component.css']
})
export class CreatepersComponent implements OnInit {
  public personne: Personnes;

  constructor(private personneService: PersonnesServices) {
    this.personne = new Personnes();
  }
  get prenom() {
    return this.monForm.get('nomPers');
  }
  get nom() {
    return this.monForm.get('prenomPers');
  }

  get mailPers() {
    return this.monForm.get('mailPers');
  }

  get dateNaisPers() {
    return this.monForm.get('dateNaisPers');
  }

  get idDossPers() {
    return this.monForm.get('idDossPers');
  }

  monForm: FormGroup;

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.monForm = new FormGroup({
      nomPers: new FormControl('', Validators.required),
      prenomPers: new FormControl('', Validators.required),
      mailPers: new FormControl('', [Validators.required]),
      dateNaisPers: new FormControl('', [Validators.required]),
      idDossPers: new FormControl('', [Validators.required])
    });
  }

  onSubmit() {
    this.personneService
      .addPersonne(this.personne)
      .subscribe(result => this.personne);
    this.createForm();
  }

}
