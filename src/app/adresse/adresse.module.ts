import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LieuxComponent } from '../lieux/lieux.component';
import { AdresseComponent } from './adresse.component';



@NgModule({
  declarations: [AdresseComponent],
  imports: [
    CommonModule
  ],
  exports: [
    LieuxComponent
  ],
  bootstrap: [AdresseComponent]
})
export class AdresseModule { }
