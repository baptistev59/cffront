import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Features } from '../shared/models/adresses/features.model';
import { AdressesService } from '../shared/services/adresses.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { __values } from 'tslib';
import { ItemAdress } from '../shared/models/adresses/itemadress.model';
import { Properties } from '../shared/models/adresses/properties.model';
import { Adresses } from '../shared/models/adresses/adresses.model';

@Component({
  selector: 'app-adresse',
  templateUrl: './adresse.component.html',
  styleUrls: ['./adresse.component.css']
})
export class AdresseComponent implements OnInit {
  public itemAdress: ItemAdress;
  public listAllFeatures: Features[];
  public listAllAdresses: Properties[];
  public listAllLabelAdresse: string[];
  monForm: FormGroup;
  public properties: Properties;
  public closeResult: string;
  public model: any;
  public selectedFeature: Features;
  public adresse: Adresses;
  public selectedOk: number;
  @Output() public addAdrr: EventEmitter<Adresses> = new EventEmitter();


  constructor(
    private adressesService: AdressesService,
    private modalService: NgbModal
  ) {
    this.itemAdress = new ItemAdress();
    this.listAllFeatures = [];
    this.listAllAdresses = [];
    this.listAllLabelAdresse = [];
    this.properties = new Properties();
    this.selectedFeature = new Features();
    this.adresse = new Adresses();
    this.selectedOk = 0;
  }

  get housenumber() {
    return this.monForm.get('housenumber');
  }
  get street() {
    return this.monForm.get('street');
  }

  get postcode() {
    return this.monForm.get('postcode');
  }

  get city() {
    return this.monForm.get('city');
  }

  get context() {
    return this.monForm.get('context');
  }

  get complement() {
    return this.monForm.get('complement');
  }

  get name() {
    return this.monForm.get('complement');
  }

  get label() {
    return this.monForm.get('label');
  }

  ngOnInit() {
    this.createForm();
  }

  onSelect(feature: Features) {
    this.selectedFeature = feature;
    this.adresse.housenumber = this.selectedFeature.properties.housenumber;
    this.adresse.street = this.selectedFeature.properties.street;
    this.adresse.complement = this.complement.value;
    this.adresse.postcode = this.selectedFeature.properties.postcode;
    this.adresse.city = this.selectedFeature.properties.city;
    this.adresse.context = this.selectedFeature.properties.context;
    this.adresse.name = this.selectedFeature.properties.name;
    this.adresse.label = this.selectedFeature.properties.label;
    this.adresse.longi = this.selectedFeature.geometry.coordinates[0];
    this.adresse.lati = this.selectedFeature.geometry.coordinates[1];
    console.log(this.selectedFeature);
    this.addAdrr.emit(this.adresse);
    this.selectedOk = 1;
  }

  createForm() {
    this.monForm = new FormGroup({
      housenumber: new FormControl('', Validators.required),
      street: new FormControl('', Validators.required),
      postcode: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      context: new FormControl('', [Validators.required]),
      complement: new FormControl(''),
      name: new FormControl('', [Validators.required]),
      longi: new FormControl(''),
      lati: new FormControl(''),
      label: new FormControl('')
    });
  }

  onSubmit() {
    this.adressesService
      .addAdresses(this.adresse)
      .subscribe(result => this.adresse);
    this.selectedOk = 0;

  }

  public findAll(search: any, postcodeSearch: any) {
    this.adressesService.findAll(search, postcodeSearch).subscribe(result => {
      this.listAllFeatures = result.features;
    });
  }
}
