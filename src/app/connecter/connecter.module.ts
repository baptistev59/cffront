import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConnecterComponent } from './connecter.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { ListpersComponent } from '../listpers/listpers.component';
import { FichepersComponent } from '../fichepers/fichepers.component';
import { CreatepersComponent } from '../createpers/createpers.component';
import { AcceuilModule } from '../acceuil/acceuil.module';
import { CreatepersModule } from '../createpers/createpers.module';
import { ParametreModule } from '../Parametre/parametre.module';
import { LoginRoutingModule } from '../login/login-routing.module';
import { routing } from '../app.routing';



@NgModule({
  declarations: [ConnecterComponent, SidebarComponent, FichepersComponent,
    CreatepersComponent,
    ListpersComponent],
  imports: [
    routing,
    BrowserModule, CommonModule, FormsModule, ReactiveFormsModule, NgbModule, AcceuilModule,
    CreatepersModule,
    ParametreModule,
    LoginRoutingModule,
  ],
  exports: [ConnecterComponent],
  bootstrap: [ConnecterComponent]
})
export class ConnecterModule { }
