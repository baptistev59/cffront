import { Component } from '@angular/core';
import {
  RouterOutlet,
  Router,
  ActivatedRoute,
  NavigationEnd
} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }
  title = 'cffront';
  showHeader = false;
  showSidebar = false;
  showFooter = false;
  loggedIn = false;
  token = '';

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnDestroy(): void {
    sessionStorage.clear();
  }

  /*recupToken() {
    axios
      .get('http://localhost:9090/auth?client=alpha&cle=@22@hr@h-CDA-alpha',
      )

      .then((data) => {
        this.token = data.data;
        sessionStorage.setItem('token', this.token);
        console.log(sessionStorage.getItem('token'));//a supprimer par la suite
      })
      .catch(console.log)


  }



  ngOnInit() {
    this.recupToken();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.showHeader =
          this.activatedRoute.firstChild.snapshot.data.showHeader !== false;
        this.showSidebar =
          this.activatedRoute.firstChild.snapshot.data.showSidebar !== false;
        this.showFooter =
          this.activatedRoute.firstChild.snapshot.data.showFooter !== false;
      }
    });
  }
}*/
}
