import { Routes, RouterModule } from '@angular/router';
import { CreatepersComponent } from './createpers/createpers.component';
import { FichepersComponent } from './fichepers/fichepers.component';
import { AuthGuard } from './shared/services/aut-guard.service';
import { ParametreComponent } from './Parametre/parametre.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { ConnecterComponent } from './connecter/connecter.component';
import { FichesessionComponent } from './fichesession/fichesession.component';
import { AdresseComponent } from './adresse/adresse.component';
import { CreatsessionComponent } from './creatsession/creatsession.component';
import { LieuxComponent } from './lieux/lieux.component';
import { LoginComponent } from './login/login.component';

const appRoutes: Routes = [
  {
    path: '',
    component: ConnecterComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'connect',
        component: LoginComponent
      },
      {
        path: 'acceuil',
        component: AcceuilComponent
      },
      {
        path: 'creapers',
        component: CreatepersComponent
      },
      {
        path: 'fichepers',
        component: FichepersComponent
      },
      {
        path: 'creasess',
        component: CreatsessionComponent
      },
      {
        path: 'fichesess',
        component: FichesessionComponent
      },
      {
        path: 'lieux',
        component: LieuxComponent
      },
      {
        path: 'adresse',
        component: AdresseComponent
      },
      {
        path: 'param',
        component: ParametreComponent
      }
    ]
  }
];

export const routing = RouterModule.forRoot(appRoutes);
