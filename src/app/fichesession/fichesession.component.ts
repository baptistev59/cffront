import { Component, OnInit, OnChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { __values } from 'tslib';
import { SessionsF } from '../shared/models/sessionsF.model';
import { SessionsServices } from '../shared/services/sessions.services';
import { FormationsService } from '../shared/services/formations.service';
import { Formations } from '../shared/models/formations.model';
import { PersonnesServices } from '../shared/services/personnes.services';
import { Personnes } from '../shared/models/personnes.model';
import { UnitesFormation } from '../shared/models/unitesformation.model';
import { UnitformationsServices } from '../shared/services/unitformations.services';

@Component({
  selector: 'app-fichesession',
  templateUrl: './fichesession.component.html',
  styleUrls: ['./fichesession.component.css']
})
export class FichesessionComponent implements OnInit, OnChanges {
  public listAllLibelleSession: string[];
  monForm: FormGroup;
  public session: SessionsF;
  public closeResult: string;
  public modelSession: any;
  public modelFormation: any;
  public modelFormateur: any;
  public modelStagiaire: any;
  public modelUForm: any;
  public selectedOk: number;
  public formationSelected: Formations;
  public formationFind: Formations;
  public listAllFormation: Formations[];
  public listAllLibelleFormation: string[];
  public formatSelectOk: number;
  public findedFormation: Formations;
  public listAllNomPers: string[];
  public listAllPersonnes: Personnes[];
  public findedPersonne: Personnes;
  public findedFormateur: Personnes;
  public findedStagiaire: Personnes;
  public listAllUform: UnitesFormation[];
  public findedUform: UnitesFormation;
  public listAllLibelleUFormation: string[];



  constructor(
    private personneService: PersonnesServices,
    private sessionsService: SessionsServices,
    private formationService: FormationsService,
    private unitformationsServices: UnitformationsServices,
    private modalService: NgbModal
  ) {
    this.selectedOk = 0;
    this.formatSelectOk = 0;
    this.formationSelected = new Formations();
    this.listAllFormation = [];
    this.listAllLibelleFormation = [];
    this.formationFind = new Formations();
    this.findedFormation = new Formations();
    this.listAllNomPers = [];
    this.listAllPersonnes = [];
    this.findedPersonne = new Personnes();
    this.findedFormateur = new Personnes();
    this.findedStagiaire = new Personnes();
    this.listAllUform = [];
    this.findedUform = new UnitesFormation();
  }

  get libelleSession() {
    return this.monForm.get('libelleSession');
  }
  get dateDebutSession() {
    return this.monForm.get('dateDebutSession');
  }

  get dateFinSession() {
    return this.monForm.get('dateFinSession');
  }

  get nbUnitesFormation() {
    return this.monForm.get('nbUnitesFormation');
  }

  get formation() {
    return this.monForm.get('formation');
  }

  get listFormateurs() {
    return this.monForm.get('listFormateurs');
  }

  get listStagiaires() {
    return this.monForm.get('listStagiaires');
  }

  get listUnitesFormation() {
    return this.monForm.get('listUnitesFormation');
  }

  ngOnInit() {
    this.createForm();
  }

  ngOnChanges(): void {
    this.listeAllLibelleSession();
  }

  createForm() {
    this.monForm = new FormGroup({
      libelleSession: new FormControl('', Validators.required),
      dateDebutSession: new FormControl('', Validators.required),
      dateFinSession: new FormControl('', [Validators.required]),
      nbUnitesFormation: new FormControl('', [Validators.required]),
      formation: new FormControl(''),
      listFormateurs: new FormControl(''),
      listStagiaires: new FormControl(''),
      listUnitesFormation: new FormControl('')
    });
  }

  listeAllLibelleSession() {
    this.sessionsService.listeAllLibelleSession().subscribe(result => {
      this.listAllLibelleSession = result;
    });
  }

  listeAllFormation() {
    this.formationService.findAll().subscribe(result => {
      this.listAllFormation = result;
      this.ListeAllLibelleFormation();
    });

  }

  ListeAllLibelleFormation() {
    this.listAllLibelleFormation = [];
    for (const lib of this.listAllFormation) {
      this.listAllLibelleFormation.push(lib.libelleFormation);
    }
  }

  ListeAllLibelleUFormation() {
    this.listAllLibelleUFormation = [];
    for (const lib of this.listAllUform) {
      console.log('listAllLibelleUFormation: ', lib);
      this.listAllLibelleUFormation.push(lib.numUF);
    }
  }

  listeAllUForm() {
    this.unitformationsServices.findAll().subscribe(result => {
      this.listAllUform = result;
      console.log('listAllUform : ', this.listAllUform);
      this.ListeAllLibelleUFormation();
    });

  }

  findByData(data: any) {
    console.log('findedByData : ', data);
    this.sessionsService.findByData(data.value).subscribe(result => {
      this.session = result;
      this.selectedOk = 1;
      console.log('findedByData : ', result);
    });

  }

  delSession() {
    console.log('delSession : ', this.session.id);
    this.sessionsService.delSession(this.session.id).subscribe(result => {
      this.session = new SessionsF();
      this.listeAllLibelleSession();
    });
  }

  listeAllNomPers() {
    this.personneService.listeAllNomPers().subscribe(result => {
      this.listAllNomPers = result;
    });
  }

  onSubmitModif() {
    this.sessionsService
      .addSession(this.session)
      .subscribe(result => this.session);
    this.selectedOk = 0;
    console.log('submit ok ! :', this.session);
  }

  onReset() {
    this.monForm.reset();
  }

  searchSession = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term =>
        term.length < 2
          ? []
          : this.listAllLibelleSession
            .filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)
            .slice(0, 10)
      )
    )

  searchFormation = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term =>
        term.length < 2
          ? []
          : this.listAllLibelleFormation
            .filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)
            .slice(0, 10)
      )
    )

  searchFormateur = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term =>
        term.length < 2
          ? []
          : this.listAllNomPers
            .filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)
            .slice(0, 10)
      )
    )

  searchStagiaire = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term =>
        term.length < 2
          ? []
          : this.listAllNomPers
            .filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)
            .slice(0, 10)
      )
    )

  searchUFormation = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term =>
        term.length < 2
          ? []
          : this.listAllLibelleUFormation
            .filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)
            .slice(0, 10)
      )
    )

  open(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(result => {
        this.closeResult = `Closed with: ${this.getReason(result)}`;
      });
  }


  private getReason(reason: any): string {
    if (reason === 'SupprSess') {
      this.delSession();
      return 'SupprSess';
    } else if (reason === 'changeFormation') {
      this.movFormation();
      return 'changeFormation';
    } else if (reason === 'AjoutFormateur') {
      this.adFormateur();
    } else if (reason === 'AjoutStagiaire') {
      this.adStagiaire();
    } else if (reason === 'AjoutUForm') {
      this.adUFormations();
      console.log('AjoutUForm : ', this.session);
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with : ${reason}`;
    }
  }

  listeAllLibelleFormation() {
    this.sessionsService.listeAllLibelleSession().subscribe(result => {
      this.listAllLibelleSession = result;
    });
  }

  listeAllPersonnes() {
    this.personneService.findAll().subscribe(result => {
      this.listAllPersonnes = result;
    });
  }

  findPersonneByData(data: any) {
    this.personneService.findByData(data.value).subscribe(result1 => {
      this.findedPersonne = result1;
      this.selectedOk = 1;
    });

  }

  selectedFormateur() {
    this.findedFormateur = this.findedPersonne;
  }

  selectedStagiaire() {
    this.findedStagiaire = this.findedPersonne;
  }

  adFormateur() {
    this.selectedFormateur();
    if (this.findedPersonne.nomPers !== '') {
      this.session.listFormateurs.push(this.findedPersonne);
      this.findedPersonne = new Personnes();
    }
  }

  delFormateur(formateur: Personnes) {
    const index: number = this.session.listFormateurs.indexOf(formateur);
    if (index !== -1) {
      this.session.listFormateurs.splice(index, 1);
    }
  }

  adStagiaire() {
    this.selectedStagiaire();
    if (this.findedPersonne.nomPers !== '') {
      this.session.listStagiaires.push(this.findedPersonne);
      this.findedPersonne = new Personnes();
    }
  }

  delStagiaire(stagiaire: Personnes) {
    const index: number = this.session.listStagiaires.indexOf(stagiaire);
    if (index !== -1) {
      this.session.listStagiaires.splice(index, 1);
    }
  }

  findFormation(form: string) {
    for (const formation of this.listAllFormation) {
      if (formation.libelleFormation === form) {
        this.findedFormation = formation;
      }
    }
    console.log('findFormation : ', this.findedFormation);
  }

  movFormation() {
    this.session.formation = this.findedFormation;
    console.log('movFormation : ', this.findedFormation);
  }

  findUFormations(uform: string) {
    for (const uformation of this.listAllUform) {
      if (uformation.numUF === uform) {
        this.findedUform = uformation;
      }
    }
    console.log('findedUform : ', this.findedUform);
  }

  adUFormations() {
    if (this.findedUform.numUF !== '') {
      this.session.listUnitesFormation.push(this.findedUform);
      console.log('adUFormations : ', this.findedUform);
    }
  }

  delUFormations(uform: UnitesFormation) {
    const index: number = this.session.listUnitesFormation.indexOf(uform);
    if (index !== -1) {
      this.session.listUnitesFormation.splice(index, 1);
    }
  }
}
