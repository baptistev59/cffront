import { Component, OnInit, OnChanges } from '@angular/core';
import { Personnes } from '../shared/models/personnes.model';
import { SessionsF } from '../shared/models/sessionsF.model';
import { PersonnesServices } from '../shared/services/personnes.services';
import { SessionsServices } from '../shared/services/sessions.services';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { __values } from 'tslib';
import { DospersService } from '../shared/services/dospers.service';
import { Dospers } from '../shared/models/dospers.model';
import { RolesService } from '../shared/services/roles.service';
import { Roles } from '../shared/models/roles.model';

@Component({
  selector: 'app-fichepers',
  templateUrl: './fichepers.component.html',
  styleUrls: ['./fichepers.component.css']
})
export class FichepersComponent implements OnInit, OnChanges {
  public model: any;
  public personne: Personnes;
  public listPersonnes: Array<Personnes>;
  public listAllNomPers: string[];
  public closeResult: string;
  public listAnimat: Array<SessionsF>;
  public listPart: Array<SessionsF>;
  public dosPers: Dospers;
  public dosPersValid: string;
  public selectedOk: number;
  public listeRole: Array<Roles>;
  monForm: FormGroup;
  public role: Roles;

  constructor(
    private personneService: PersonnesServices,
    private sessionsService: SessionsServices,
    private rolesService: RolesService,
    private dospersService: DospersService,
    private modalService: NgbModal
  ) {
    this.personne = new Personnes();
    this.listPersonnes = [];
    this.listAnimat = [];
    this.listPart = [];
    this.dosPersValid = 'false';
    this.selectedOk = 0;
    this.dosPers = new Dospers();
    this.listeRole = [];
    this.role = new Roles();
  }

  get prenom() {
    return this.monForm.get('nomPers');
  }
  get nom() {
    return this.monForm.get('prenomPers');
  }

  get mailPers() {
    return this.monForm.get('mailPers');
  }

  get dateNaisPers() {
    return this.monForm.get('dateNaisPers');
  }

  get idDossPers() {
    return this.monForm.get('idDossPers');
  }

  get listRoles() {
    return this.monForm.get('listRoles');
  }

  ngOnInit() {
    this.createForm();
  }

  ngOnChanges() {
    this.listeAllNomPers();
  }

  createForm() {
    this.monForm = new FormGroup({
      nomPers: new FormControl('', Validators.required),
      prenomPers: new FormControl('', Validators.required),
      mailPers: new FormControl('', [Validators.required]),
      dateNaisPers: new FormControl('', [Validators.required]),
      idDossPers: new FormControl('', [Validators.required]),
      listRoles: new FormControl('')
    });
  }

  findAll() {
    this.personneService.findAll().subscribe(result => {
      this.listPersonnes = result;
    });
  }

  listeAllNomPers() {
    this.personneService.listeAllNomPers().subscribe(result => {
      this.listAllNomPers = result;
    });
  }

  findById(find: any) {
    console.log(find.value);
    this.personneService.findById(find.value).subscribe(result => {
      this.personne = result;
    });
  }

  findByNomPers(find: any) {
    console.log(find.value);
    this.personneService.findByNomPers(find.value).subscribe(result => {
      this.personne = result;
    });
  }

  delPersonne() {
    console.log(this.personne.id);
    this.personneService.delPersonne(this.personne.id).subscribe(result => {
      this.personne = new Personnes();
      this.listeAllNomPers();
    });
  }

  delPersonneByNomPers(find: any) {
    console.log(find.value);
    this.personneService.delPersonneByNomPers(find.value).subscribe(result => {
      this.personne = new Personnes();
    });
  }

  findByData(data: any) {
    this.personneService.findByData(data.value).subscribe(result1 => {
      this.personne = result1;
      this.findSessionByPers(result1.id);
      this.findDosByPers(result1);
      this.selectedOk = 1;
    });
  }

  findSessionByPers(id: number) {
    this.listeRole = new Array();
    this.sessionsService.findByIdForm(id).subscribe(result2 => {
      this.listAnimat = result2;
      if (this.listAnimat.length > 0) {
        this.personne.listAnimat = this.listAnimat;
        this.AddRoleFormateur();
      } else {
        this.personne.listAnimat = null;
      }
    });
    this.sessionsService.findByIdStag(id).subscribe(result3 => {
      this.listPart = result3;
      if (this.listPart.length > 0) {
        this.personne.listPart = this.listPart;
        this.AddRoleStagiaire();
      } else {
        this.personne.listPart = null;
      }
    });
  }

  public findDosByPers(personne: Personnes) {
    this.dospersService.findDosByPers(personne).subscribe(result => {
      this.dosPers = result;
      if (result !== null) {
        this.dosPersValid = 'true';
      } else {
        this.dosPersValid = 'false';
      }
    });
  }

  AddRoleStagiaire() {
    if (this.personne.listPart) {
      if (this.personne.listPart.length > 0) {
        this.rolesService.findByLibelleRole('Stagiaire').subscribe(result => {
          this.role = result;
          this.listeRole.push(this.role);
          console.log('if stagiaire ' + this.role.libeleRole);
        });

      }
    }
    this.personne.listRoles = this.listeRole;
  }

  AddRoleFormateur() {
    if (this.personne.listAnimat) {
      if (this.personne.listAnimat.length > 0) {
        this.rolesService.findByLibelleRole('Formateur').subscribe(result => {
          this.role = result;
          this.listeRole.push(this.role);
          console.log('if Formateur ' + this.role.libeleRole);
        });
      }
    }
    this.personne.listRoles = this.listeRole;
  }
  onSubmit() {
    this.personneService
      .addPersonne(this.personne)
      .subscribe(result => this.personne);
    this.selectedOk = 0;
  }

  onReset() {
    this.monForm.reset();
    this.listRoles.reset();
    this.listAnimat = [];
    this.listPart = [];
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term =>
        term.length < 2
          ? []
          : this.listAllNomPers
            .filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)
            .slice(0, 10)
      )
    )

  open(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(result => {
        this.closeResult = `Closed with: ${this.getReason(result)}`;
      });
  }
  private getReason(reason: any): string {
    if (reason === 'Ok') {
      this.delPersonne();
      return 'Ok';
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with : ${reason}`;
    }
  }
}
