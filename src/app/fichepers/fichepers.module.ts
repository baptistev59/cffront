import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule, NgbModalModule } from "@ng-bootstrap/ng-bootstrap";
import { FichepersComponent } from "./fichepers.component";

@NgModule({
  declarations: [FichepersComponent],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgbModalModule
  ],
  exports: [FichepersComponent],
  bootstrap: [FichepersComponent]
})
export class FichepersModule {}
