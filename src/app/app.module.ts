import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { PersonnesServices } from './shared/services/personnes.services';
import { UtilisateurServices } from './shared/services/utilisateur.services';
import { HttpClientModule } from '@angular/common/http';
import { CreatepersModule } from './createpers/createpers.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { routing } from './app.routing';
import { AuthGuard } from './shared/services/aut-guard.service';
import { CentreServices } from './shared/services/centre.services';
import { ParametreModule } from './Parametre/parametre.module';
import { AcceuilModule } from './acceuil/acceuil.module';
import { ConnecterModule } from './connecter/connecter.module';
import { CreatsessionComponent } from './creatsession/creatsession.component';
import { FichesessionComponent } from './fichesession/fichesession.component';
import { AuthService } from './shared/services/auth.service';
import { ContentModule } from './content/content.module';
import { LieuxComponent } from './lieux/lieux.component';
import { LieuxModule } from './lieux/lieux.module';
import { LieuxService } from './shared/services/lieux.service';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    ContentComponent,
    CreatsessionComponent,
    FichesessionComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    AcceuilModule,
    ContentModule,
    CreatepersModule,
    ConnecterModule,
    ParametreModule,
    LieuxModule,
    routing
  ],
  providers: [
    PersonnesServices,
    UtilisateurServices,
    AuthService,
    AuthGuard,
    CentreServices,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
