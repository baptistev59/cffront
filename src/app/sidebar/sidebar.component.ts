import { Component, OnInit, OnChanges } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  providers: [NgbAccordionConfig]
})
export class SidebarComponent implements OnInit, OnChanges {
  auth: boolean;
  constructor(private authService: AuthService, config: NgbAccordionConfig) {
    this.auth = this.authService.isloggedIn;
    config.closeOthers = true;

  }

  ngOnInit() {
    this.auth = this.authService.isloggedIn;
  }

  ngOnChanges() {
    this.auth = this.authService.isloggedIn;
  }

  logOut() {
    this.authService.logOut();
  }
}
