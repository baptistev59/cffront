import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../shared/models/utilisateur.model';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  public utilisateur: Utilisateur = new Utilisateur();

  constructor() { }

  ngOnInit() {
  }
}
