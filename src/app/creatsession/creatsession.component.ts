import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionsF } from '../shared/models/sessionsF.model';
import { SessionsServices } from '../shared/services/sessions.services';

@Component({
  selector: 'app-creatsession',
  templateUrl: './creatsession.component.html',
  styleUrls: ['./creatsession.component.css']
})
export class CreatsessionComponent implements OnInit {
  public session: SessionsF;
  monForm: FormGroup;


  constructor(
    private sessionsService: SessionsServices) {
    this.session = new SessionsF();
  }

  get libelleSession() {
    return this.monForm.get('libelleSession');
  }
  get dateDebutSession() {
    return this.monForm.get('dateDebutSession');
  }

  get dateFinSession() {
    return this.monForm.get('dateFinSession');
  }

  get nbUnitesFormation() {
    return this.monForm.get('nbUnitesFormation');
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.monForm = new FormGroup({
      libelleSession: new FormControl('', Validators.required),
      dateDebutSession: new FormControl('', Validators.required),
      dateFinSession: new FormControl('', [Validators.required]),
      nbUnitesFormation: new FormControl('')
    });
  }

  onSubmit() {
    this.sessionsService
      .addSession(this.session)
      .subscribe(result => this.session);
    console.log('submit ok !');
    this.createForm();
  }
}
