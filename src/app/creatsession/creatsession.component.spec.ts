import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatsessionComponent } from './creatsession.component';

describe('CreatsessionComponent', () => {
  let component: CreatsessionComponent;
  let fixture: ComponentFixture<CreatsessionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatsessionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatsessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
