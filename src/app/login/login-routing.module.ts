import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';
import { LoginComponent } from './login.component';
import { AuthGuard } from '../shared/services/aut-guard.service';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: 'login', component: LoginComponent }
        ])
    ],
    exports: [
        RouterModule
    ],
    providers: [
        AuthService,
        AuthGuard
    ]
})
export class LoginRoutingModule { }
