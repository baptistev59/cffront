import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';

@Component({

  // tslint:disable-next-line: component-selector
  selector: 'login',
  styleUrls: ['./login.component.css'],
  templateUrl: './login.component.html',
})
export class LoginComponent {
  message = 'Vous êtes déconnecté !';
  private name: string;
  private password: string;

  constructor(private authService: AuthService, private router: Router) { }

  // Informe l'utilisateur sur son authentfication.
  setMessage() {
    this.message = this.authService.isloggedIn ?
      'Vous êtes connecté.' : 'Identifiant ou mot de passe incorrect.';
  }

  // Connecte l'utilisateur auprès du Guard
  login() {
    this.message = 'Connexion en cours ...';

    this.authService.logIn(this.name, this.password).subscribe(() => {
      this.setMessage();
      console.log('isloggedIn : ' + (this.authService.isloggedIn));
      if (this.authService.isloggedIn) {
        // Récupère l'URL de redirection depuis le service d'authentification
        // Si aucune redirection n'a été définis, redirige l'utilisateur vers l'accueil'.
        const redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '';
        // Redirige l'utilisateur
        this.router.navigate([redirect]);
      } else {
        this.password = '';
      }
    });
  }

  // Déconnecte l'utilisateur
  logout() {
    this.authService.logOut();
    this.setMessage();
  }
}
