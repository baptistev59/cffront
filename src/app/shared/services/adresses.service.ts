import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Features } from '../models/adresses/features.model';
import { ItemAdress } from '../models/adresses/itemadress.model';
import { Adresses } from '../models/adresses/adresses.model';

@Injectable({
  providedIn: 'root'
})
export class AdressesService {
  private banUrl: string;
  private adressesUrl: string;
  public listAllLabelAdresse: string[];
  public listAllAdresse: Features[];

  constructor(private http: HttpClient) {
    this.banUrl = 'https://api-adresse.data.gouv.fr/search/';
    this.adressesUrl = 'http://ladresseduwebservice';
  }

  public findAll(search: any, postcodeSearch: any): Observable<ItemAdress> {
    console.log(this.banUrl + '?q= '
      + search + '&postcode=' + postcodeSearch
      + '&limit=15');
    return this.http.get<ItemAdress>(this.banUrl + '?q= '
      + search + '&postcode=' + postcodeSearch
      + '&limit=15');
  }

  public addAdresses(adresse: Adresses): Observable<Adresses> {
    return this.http.post<Adresses>(this.adressesUrl, adresse);
  }

}
