import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import { Utilisateur } from '../models/utilisateur.model';
import { HttpClient } from '@angular/common/http';



@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private utilisateur: Utilisateur = new Utilisateur();
    private utilisateurFinded: Utilisateur = null;
    isloggedIn = false; // L'utilisateur est-il connecté ?
    redirectUrl: string; // Où rediriger l'utilisateur suite à la connexion
    private utilisateurUrl: string;

    constructor(private http: HttpClient) {
        this.utilisateurUrl = 'http://51.254.221.245:8082/cfback/Users';
    }

    public connexion(utilisateur: Utilisateur): Observable<Utilisateur> {
        return this.http.post<Utilisateur>(this.utilisateurUrl + '/find', utilisateur);

    }

    logIn(username: string, password: string) {

        this.utilisateur.username = username;
        this.utilisateur.password = password;

        this.connexion(this.utilisateur).subscribe(result => {
            this.utilisateurFinded = result;

        });

        const isLoggedIn = (this.utilisateurFinded != null);
        return Observable.of(true).delay(1000).do(val => this.isloggedIn = (this.utilisateurFinded != null));

        // return new Promise(
        //     (resolve, reject) => {
        //         setTimeout(
        //             () => {
        //                 this.isloggedIn = isLoggedIn;
        //                 resolve(true);
        //             }, 2000
        //         );
        //     }
        // );
    }


    logOut() {
        this.isloggedIn = false;
    }
}
