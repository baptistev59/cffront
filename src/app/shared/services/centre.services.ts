import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Centre } from '../models/centre.model';

@Injectable(
  { providedIn: 'root' }
)
export class CentreServices {
  private centreUrl: string;

  constructor(private http: HttpClient) {
    this.centreUrl = 'http://51.254.221.245:8082/cfback/Centre';
  }

  public findById(id: number): Observable<Centre> {
    return this.http.get<Centre>(this.centreUrl + '/' + id);
  }

  public addCentre(centre: Centre): Observable<Centre> {
    return this.http.post<Centre>(this.centreUrl, centre);
  }

  public delCentre(id: number): Observable<Centre> {
    return this.http.delete<Centre>(this.centreUrl + '/' + id);
  }
}
