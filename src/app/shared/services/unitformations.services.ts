import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Formations } from '../models/formations.model';
import { SessionsF } from '../models/sessionsF.model';
import { UnitesFormation } from '../models/unitesformation.model';

@Injectable({
  providedIn: 'root'
})
export class UnitformationsServices {
  private uformationsUrl: string;

  constructor(private http: HttpClient) {
    this.uformationsUrl = 'http://51.254.221.245:8082/cfback/UnitesFormation';
  }

  public findAll(): Observable<UnitesFormation[]> {
    return this.http.get<UnitesFormation[]>(this.uformationsUrl);
  }

  public findBySession(session: SessionsF): Observable<UnitesFormation> {
    return this.http.post<UnitesFormation>(this.uformationsUrl + '/findsession', session);
  }

  public findById(unitesformation: UnitesFormation): Observable<UnitesFormation> {
    return this.http.post<UnitesFormation>(this.uformationsUrl + '/', unitesformation.id);
  }

  public addFormation(uFormation: UnitesFormation): Observable<UnitesFormation> {
    return this.http.post<UnitesFormation>(this.uformationsUrl, uFormation);
  }

}
