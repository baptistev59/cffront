import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Formations } from '../models/formations.model';
import { Observable } from 'rxjs';
import { SessionsF } from '../models/sessionsF.model';

@Injectable({
  providedIn: 'root'
})
export class FormationsService {
  private formationsUrl: string;

  constructor(private http: HttpClient) {
    this.formationsUrl = 'http://51.254.221.245:8082/cfback/Formations';
  }

  public findAll(): Observable<Formations[]> {
    return this.http.get<Formations[]>(this.formationsUrl);
  }

  public findBySession(session: SessionsF): Observable<Formations> {
    return this.http.post<Formations>(this.formationsUrl + '/findsession', session);
  }

  public addFormation(formation: Formations): Observable<Formations> {
    return this.http.post<Formations>(this.formationsUrl, formation);
  }


}
