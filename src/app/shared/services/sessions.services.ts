import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SessionsF } from '../models/sessionsF.model';

@Injectable({
  providedIn: 'root'
})
export class SessionsServices {
  private sessionsUrl: string;

  constructor(private http: HttpClient) {
    this.sessionsUrl = 'http://51.254.221.245:8082/cfback/Sessions';
  }

  public findAll(): Observable<SessionsF[]> {
    return this.http.get<SessionsF[]>(this.sessionsUrl);
  }

  public findById(id: number): Observable<SessionsF> {
    return this.http.get<SessionsF>(this.sessionsUrl + '/findid=' + id);
  }

  public findByIdForm(id: number): Observable<SessionsF[]> {
    return this.http.get<SessionsF[]>(this.sessionsUrl + '/form=' + id);
  }
  public findByIdStag(id: number): Observable<SessionsF[]> {
    return this.http.get<SessionsF[]>(this.sessionsUrl + '/stag=' + id);
  }

  public listeAllLibelleSession(): Observable<string[]> {
    return this.http.get<string[]>(this.sessionsUrl + '/listlibsess');
  }

  public findByData(data: string): Observable<SessionsF> {
    return this.http.post<SessionsF>(this.sessionsUrl + '/finddatap', data);
  }

  public addSession(session: SessionsF): Observable<SessionsF> {
    return this.http.post<SessionsF>(this.sessionsUrl, session);
  }

  public delSession(id: number): Observable<SessionsF> {
    return this.http.delete<SessionsF>(this.sessionsUrl + '/delid=' + id);
  }
}
