import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Utilisateur } from '../models/utilisateur.model';

@Injectable()
export class UtilisateurServices {
  private utilisateurUrl: string;

  constructor(private http: HttpClient) {
    this.utilisateurUrl = 'http://51.254.221.245:8082/cfback/Users';
  }

  public connexion(utilisateur: Utilisateur): Observable<Utilisateur> {
    return this.http.post<Utilisateur>(this.utilisateurUrl + '/find', utilisateur);
  }
}
