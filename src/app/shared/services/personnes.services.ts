import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Personnes } from '../models/personnes.model';

@Injectable()
export class PersonnesServices {
  private personnesUrl: string;
  public newData: string;

  constructor(private http: HttpClient) {
    this.personnesUrl = 'http://51.254.221.245:8082/cfback/Personnes';
  }

  public findAll(): Observable<Personnes[]> {
    return this.http.get<Personnes[]>(this.personnesUrl);
  }

  public findById(id: number): Observable<Personnes> {
    return this.http.get<Personnes>(this.personnesUrl + '/findid=' + id);
  }

  public findByNomPers(nomPers: string): Observable<Personnes> {
    return this.http.get<Personnes>(this.personnesUrl + '/findnom=' + nomPers);
  }

  public addPersonne(personne: Personnes): Observable<Personnes> {
    return this.http.post<Personnes>(this.personnesUrl, personne);
  }

  public delPersonne(id: number): Observable<Personnes> {
    return this.http.delete<Personnes>(this.personnesUrl + '/delid=' + id);
  }

  public delPersonneByNomPers(nomPers: string): Observable<Personnes> {
    return this.http.delete<Personnes>(
      this.personnesUrl + '/delnom=' + nomPers
    );
  }

  public listeAllNomPers(): Observable<string[]> {
    return this.http.get<string[]>(this.personnesUrl + '/listNomPers');
  }

  public findByData(data: string): Observable<Personnes> {
    return this.http.post<Personnes>(this.personnesUrl + '/finddatap', data);
  }
}
