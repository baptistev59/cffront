import { Injectable } from '@angular/core';
import { Lieux } from '../models/lieux.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LieuxService {
  private lieuxUrl: string;

  constructor(private http: HttpClient) {
    this.lieuxUrl = 'http://51.254.221.245:8082/cfback/Lieux';
  }

  public findAll(): Observable<Lieux[]> {
    return this.http.get<Lieux[]>(this.lieuxUrl);
  }

  public findById(id: number): Observable<Lieux> {
    return this.http.get<Lieux>(this.lieuxUrl + '/' + id);
  }

  public addLieux(lieux: Lieux): Observable<Lieux> {
    return this.http.post<Lieux>(this.lieuxUrl + '/addLieu', lieux);
  }

  public delLieux(id: number): Observable<Lieux> {
    return this.http.delete<Lieux>(this.lieuxUrl + '/' + id);
  }
}
