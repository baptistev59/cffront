import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dospers } from '../models/dospers.model';
import { Personnes } from '../models/personnes.model';

@Injectable({
  providedIn: 'root'
})
export class DospersService {
  private sessionsUrl: string;

  constructor(private http: HttpClient) {
    this.sessionsUrl = 'http://51.254.221.245:8084/sfback/personne';
  }

  public findDosByPers(personne: Personnes): Observable<Dospers> {
    return this.http.get<Dospers>(
      this.sessionsUrl +
      '?numeroDossier=' +
      personne.idDossPers +
      '&nom=' +
      personne.nomPers +
      '&prenom=' +
      personne.prenomPers
    );
  }
}
