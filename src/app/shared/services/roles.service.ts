import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Roles } from '../models/roles.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  private rolesUrl: string;

  constructor(private http: HttpClient) {
    this.rolesUrl = 'http://51.254.221.245:8082/cfback/Roles';
  }

  public findAll(): Observable<Roles[]> {
    return this.http.get<Roles[]>(this.rolesUrl);
  }

  public findByLibelleRole(lib: string): Observable<Roles> {
    return this.http.get<Roles>(this.rolesUrl + '/lib=' + lib);
  }
}
