import { Competences } from './competences.model';
import { Adresses } from './adresses/adresses.model';

export class Dospers {
  constructor(
    public idPersonne: number = 0,
    public nom: string = '',
    public prenom: string = '',
    public mail: string = '',
    public telephone: string = '0',
    public dateDeNais: Date = null,
    public lieuDeNais: Date = null,
    public dispo: string = '',
    public numeroDossier: string = 'Interne',
    public adresse: Adresses = null,
    public competencesList: Array<Competences> = [],
    public ficheSante: Array<string> = [],
    public tuteur: Array<string> = []
  ) { }
}
