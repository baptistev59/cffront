export class Adresses {
    constructor(
        public label: string = '',
        public housenumber: string = '',
        public name: string = '',
        public street: string = '',
        public postcode: string = '',
        public city: string = '',
        public context: string = '',
        public longi: number = 0,
        public lati: number = 0,
        public complement: string = ''
    ) { }
}
