import { Properties } from './properties.model';
import { Geometry } from './geometry.model';

export class Features {
    constructor(
        public properties: Properties = null,
        public geometry: Geometry = null
    ) { }
}