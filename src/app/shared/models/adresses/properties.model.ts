export class Properties {
    constructor(
        public label: string = '',
        public housenumber: string = '',
        public street: string = '',
        public postcode: string = '',
        public city: string = '',
        public context: string = '',
        public x: number = 0,
        public y: number = 0,
        public name: string = ''
    ) { }
}
