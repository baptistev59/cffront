import { Features } from './features.model';

export class ItemAdress {
    constructor(
        public attribution: string = '',
        public licence: string = '',
        public query: string = '',
        public limit: number = 0,
        public features: Features[] = null,
        public version: string = '',
        public type: string = ''
    ) { }
}
