import { Utilisateur } from './utilisateur.model';
import { Lieux } from './lieux.model';

export class Centre {
    constructor(
        public id: number = 0,
        public libelle: string = '',
        public slogan: string = '',
        public tel: string = '',
        public fax: string = '',
        public email: string = '',
        public site: string = '',
        public siret: string = '',
        public lieu: Lieux = null,
        public listUsers: Array<Utilisateur> = []
    ) { }
}
