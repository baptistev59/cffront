import { Personnes } from './personnes.model';
import { UnitesFormation } from './unitesformation.model';
import { Formations } from './formations.model';

export class SessionsF {
  constructor(
    public id: number = 0,
    public dateDebutSession: Date = null,
    public dateFinSession: Date = null,
    public nbUnitesFormation: number = 0,
    public libelleSession: string = '',
    public listFormateurs: Array<Personnes> = [],
    public listStagiaires: Array<Personnes> = [],
    public listUnitesFormation: Array<UnitesFormation> = [],
    public formation: Formations = null
  ) { }
}
