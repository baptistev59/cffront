import { Personnes } from "./personnes.model";

export class Presences {
  constructor(
    public id: number,
    public etatPresence: boolean,
    public signaturePresence: boolean,
    public validPresence: boolean,
    public uniteFormation: boolean,
    public formateur: Personnes,
    public stagiaire: Personnes
  ) {}
}
