import { SessionsF } from './sessionsF.model';
import { Roles } from './roles.model';

export class Personnes {
  constructor(
    public id: number = 0,
    public nomPers: string = '',
    public prenomPers: string = '',
    public mailPers: string = '',
    public dateNaisPers: Date = null,
    public idDossPers: string = '',
    public listAnimat: Array<SessionsF> = [],
    public listPart: Array<SessionsF> = [],
    public listRoles: Array<Roles> = []
  ) { }
}
