import { SessionsF } from "./sessionsF.model";
import { Formations } from "./formations.model";

export class Competences {
  constructor(
    public id: number,
    public libelleCompetence: string,
    public detailsCompetence: string,
    public listSessions: Array<SessionsF>,
    public formation: Formations
  ) {}
}
