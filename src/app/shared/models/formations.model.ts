import { SessionsF } from './sessionsF.model';
import { Competences } from './competences.model';
import { Categories } from './categories.model';

export class Formations {
  constructor(
    public id: number = 0,
    public libelleFormation: string = '',
    public detailsFormation: string = '',
    public listSessions: Array<SessionsF> = [],
    public categorie: Categories = null,
    public listCompetences: Competences = null
  ) { }
}
