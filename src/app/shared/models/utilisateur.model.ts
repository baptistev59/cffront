export class Utilisateur {
    constructor(
        public id: number = 0,
        public username: string = '',
        public password: string = '',
        public firstName: string = '',
        public lastName: string = '',
        public token: string = '',
    ) { }
}
