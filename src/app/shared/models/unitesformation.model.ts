import { Lieux } from './lieux.model';
import { Presences } from './presences.model';
import { SessionsF } from './sessionsF.model';

export class UnitesFormation {
  constructor(
    public id: number = 0,
    public numUF: string = '',
    public session: SessionsF = null,
    public listPresences: Array<Presences> = null,
    public lieu: Lieux = null
  ) { }
}
