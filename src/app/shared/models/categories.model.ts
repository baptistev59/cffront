export class Categories {
  constructor(
    public id: number,
    public libelleCategorie: string,
    public detailsCategorie: string
  ) {}
}
