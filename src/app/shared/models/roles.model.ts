import { Personnes } from './personnes.model';

export class Roles {
    constructor(
        public id: number = 0,
        public libeleRole: string = '',
        public detailsRole: string = '',
        public listPersonnes: Array<Personnes> = null
    ) { }
}
