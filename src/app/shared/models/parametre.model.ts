import { Utilisateur } from './utilisateur.model';

export class Parametre {
    constructor(
        public id: number,
        public p1: string,
        public p2: string,
        public p3: string,
        public param: Array<Utilisateur>
    ) { }
}
