import { Adresses } from './adresses/adresses.model';

export class Lieux {
  constructor(
    public id: number = 0,
    public libelleLieu: string = '',
    public detailsLieu: string = '',
    public adresse: Adresses = null,
  ) {

  }
}
