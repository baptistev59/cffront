import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendrierComponent } from './calendrier.component';


@NgModule({
  declarations: [CalendrierComponent],
  imports: [
    BrowserModule, CommonModule, FormsModule, ReactiveFormsModule, NgbModule
  ],
  exports: [CalendrierComponent],
  bootstrap: [CalendrierComponent]
})
export class CalendrierModule { }
