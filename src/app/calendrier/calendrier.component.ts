import { Component, OnInit, OnChanges } from '@angular/core';
import { Centre } from '../shared/models/centre.model';
@Component({
  selector: 'app-calendrier',
  templateUrl: './calendrier.component.html',
  styleUrls: ['./calendrier.component.css']
})
export class CalendrierComponent implements OnInit {
  public model: any;
  public centre: Centre;
  public today = new Date();
  public currentMonth = this.today.getMonth();
  public currentYear = this.today.getFullYear();
  public selectYear = document.getElementById('year');
  public selectMonth = document.getElementById('month');

  public months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  public monthAndYear = document.getElementById('monthAndYear');



  constructor() { }

  ngOnInit() {

    this.monthAndYear = document.getElementById('monthAndYear');
    this.selectYear = document.getElementById('year');
    this.selectMonth = document.getElementById('month');
    this.showCalendar(this.currentMonth, this.currentYear);
  }

  next() {
    this.currentYear = (this.currentMonth === 11) ? this.currentYear + 1 : this.currentYear;
    this.currentMonth = (this.currentMonth + 1) % 12;
    this.showCalendar(this.currentMonth, this.currentYear);
  }

  previous() {
    this.currentYear = (this.currentMonth === 0) ? this.currentYear - 1 : this.currentYear;
    this.currentMonth = (this.currentMonth === 0) ? 11 : this.currentMonth - 1;
    this.showCalendar(this.currentMonth, this.currentYear);
  }

  jump() {
    // tslint:disable-next-line: radix
    this.currentYear = parseInt((this.selectYear as HTMLInputElement).value);
    // tslint:disable-next-line: radix
    this.currentMonth = parseInt((this.selectMonth as HTMLInputElement).value);
    this.showCalendar(this.currentMonth, this.currentYear);
  }

  showCalendar(month, year) {

    const firstDay = (new Date(year, month)).getDay();
    const daysInMonth = 32 - new Date(year, month, 32).getDate();

    const tbl = document.getElementById('calendar-body'); // body of the calendar

    // clearing all previous cells
    tbl.innerHTML = '';

    // filing data about month and in the page via DOM.
    this.monthAndYear.innerHTML = this.months[month] + ' ' + year;
    this.selectYear = year;
    this.selectMonth = month;

    // creating all cells
    let date = 1;
    for (let i = 0; i < 6; i++) {
      // creates a table row
      const row = document.createElement('tr');

      // creating individual cells, filing them up with data.
      for (let j = 0; j < 7; j++) {
        if (i === 0 && j < firstDay) {
          const cell = document.createElement('td');
          const cellText = document.createTextNode('');
          cell.appendChild(cellText);
          row.appendChild(cell);
        } else if (date > daysInMonth) {
          break;
        } else {
          const cell = document.createElement('td');
          const cellText = document.createTextNode(date.toString());
          if (date === this.today.getDate() && year === this.today.getFullYear() && month === this.today.getMonth()) {
            cell.classList.add('bg-info');
          } // color today's date
          cell.appendChild(cellText);
          row.appendChild(cell);
          date++;
        }


      }
      tbl.appendChild(row);
    }
  }
}
