import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdresseComponent } from '../adresse/adresse.component';
import { LieuxComponent } from './lieux.component';
import { AdresseModule } from '../adresse/adresse.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LieuxService } from '../shared/services/lieux.service';


@NgModule({
  declarations: [LieuxComponent, AdresseComponent],
  imports: [
    BrowserModule, CommonModule, CommonModule, FormsModule, ReactiveFormsModule, NgbModule
  ],
  exports: [LieuxComponent],
  providers: [LieuxService],
  bootstrap: [LieuxComponent]
})
export class LieuxModule { }
