import { Component, OnInit, Input } from '@angular/core';
import { Lieux } from '../shared/models/lieux.model';
import { LieuxService } from '../shared/services/lieux.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Adresses } from '../shared/models/adresses/adresses.model';
import { AdresseModule } from '../adresse/adresse.module';

@Component({
  selector: 'app-lieux',
  templateUrl: './lieux.component.html',
  styleUrls: ['./lieux.component.css']
})
export class LieuxComponent implements OnInit {
  public lieux: Lieux;
  public adresse: Adresses;


  constructor(private lieuxService: LieuxService) {
    this.lieux = new Lieux();
  }
  get libelleLieu() {
    return this.monForm.get('libelleLieu');
  }
  get detailsLieu() {
    return this.monForm.get('detailsLieu');
  }

  monForm: FormGroup;

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.monForm = new FormGroup({
      libelleLieu: new FormControl('', Validators.required),
      detailsLieu: new FormControl('', Validators.required),

    });
  }

  ajoutAdrr(adresse: Adresses) {
    this.lieux.adresse = adresse;
    console.log(adresse.label);
    console.log(adresse);
  }
  onSubmit() {
    this.lieuxService
      .addLieux(this.lieux)
      .subscribe(result => this.lieux);
    this.createForm();
    console.log('this.lieux : ' + this.lieux);
  }


}
