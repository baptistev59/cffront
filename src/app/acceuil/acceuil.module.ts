import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AcceuilComponent } from './acceuil.component';
import { CalendrierModule } from '../calendrier/calendrier.module';



@NgModule({
  declarations: [AcceuilComponent],
  imports: [
    BrowserModule,
    CalendrierModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  exports: [AcceuilComponent],
  bootstrap: [AcceuilComponent]
})
export class AcceuilModule { }
